# Program name:   Asg3aEx1.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     3A
# Exercise:       #1
# Purpose:        To ask for the average temperature in a given month (November through February) and then
#                 calculate the average of the given values


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# Define variables
# The prompts are nearly identical, so let's assign the repeated part to a string variable
prompt_template = "What was the average temperature (in Fahrenheit) for "

# Get user input
# Used like this, + is a string concatenation operator
puts prompt_template + "November?"
november = gets

puts prompt_template + "December?"
december = gets

puts prompt_template + "January?"
january = gets

puts prompt_template + "February?"
february = gets

# Calculate average (sum of months / number of months)
# .to_f converts the string value to a floating point number
average = (november.to_f + december.to_f + january.to_f + february.to_f) / 4.0

# Display calculated average
# .to_s converts the number to a string
puts "The average winter temperature was " + average.to_s + " degrees Fahrenheit."