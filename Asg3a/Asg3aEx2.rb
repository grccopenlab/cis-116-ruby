# Program name:   Asg3aEx2.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     3A
# Exercise:       #2
# Purpose:        To calculate the mileage (MPG) for a vehicle


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# Get user input
puts "How far (in miles) did the vehicle drive? "
miles = gets

puts "How many gallons did the vehicle use? "
gallons = gets

# Calculate miles per gallon (mpg) (miles traveled / gallons used)
# .to_f converts the string value to a floating point number
mpg = miles.to_f / gallons.to_f

# Display calculated average
# .to_s converts the number to a string
puts "The gas mileage for the vehicle was " + mpg.to_s + " MPG"
