# Program name:   Asg3aEx3.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     3A
# Exercise:       #3
# Purpose:        To calculate the cost to fertilize a rectangular plot of land


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

puts "All measurements should be entered in feet. Cost should be entered in dollars."
puts ""

# Get user input
puts "How wide is the lot? "
lot_width = gets

puts "How long is the lot? "
lot_length = gets


puts "How wide is the house? "
house_width = gets

puts "How long is the house? "
house_length = gets


puts "How wide is the driveway? "
driveway_width = gets

puts "How long is the driveway? "
driveway_length = gets


puts "How much does a bag of fertilizer cost? "
fertilizer_cost = gets

puts "How many square feet does a bag of fertilizer cover? "
fertilizer_area = gets

# Calculate the area of the lot, the house, and the driveway
lot_area = lot_length.to_f * lot_width.to_f
house_area = house_length.to_f * house_width.to_f
driveway_area = driveway_length.to_f * driveway_width.to_f


# Area to cover is the lot area minus the house area and minus the driveway area
area_to_cover = lot_area - house_area - driveway_area

# Bags needed is the upward-rounded result of dividing the area to cover by
# the area a bag of fertilizer covers
# Calling to_i on a floating point number truncates the decimal point
bags_needed = (area_to_cover / fertilizer_area.to_f).to_i

# Cost to fertilize the lot is the cost per bag times the number of bags needed
cost = fertilizer_cost.to_f * bags_needed

puts "The cost to fertilize the yard is $" + cost.to_s
