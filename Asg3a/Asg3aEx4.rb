# Program name:   Asg3aEx4.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     3A
# Exercise:       #4
# Purpose:        To calculate the tip and final cost for a meal


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# Get user input
puts "How much did the meal cost? "
base_cost = gets

puts "What percentage tip would you like to leave (i.e. enter 15 for 15%)? "
tip_value = gets

# Convert tip value to a decimal percent
tip_percentage = tip_value.to_f / 100

# Tip amount is the cost of the meal times the tip percentage
tip_amount = base_cost.to_f * tip_percentage

# Total cost of the meal (assuming no tax) is the cost of the meal plus the tip amount
total_cost = base_cost.to_f + tip_amount

# Display tip amount
puts "Your tip will be $" + tip_amount.to_s

# Display final cost
puts "Your final bill will be $" + total_cost.to_s
