# Program name:   Asg3aEx5.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     3A
# Exercise:       #5
# Purpose:        To calculate a student's body mass index (BMI)


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

# Constants
CENTIMETERS_PER_INCH = 2.54
CENTIMETERS_PER_METER = 100
GRAMS_PER_POUND = 453.59
GRAMS_PER_KILOGRAM = 1000


# Get user input
puts "How tall are you (in inches)? "
height_in_inches = gets

puts "How much do you weigh (in pounds)? "
weight_in_pounds = gets


# Convert inches to centimeters
height_in_centimeters = CENTIMETERS_PER_INCH * height_in_inches.to_f

# Convert centimeters to meters
height_in_meters = height_in_centimeters / CENTIMETERS_PER_METER


# Convert pounds to grams
weight_in_grams = GRAMS_PER_POUND * weight_in_pounds.to_f

# Convert grams to kilograms
weight_in_kilograms = weight_in_grams / GRAMS_PER_KILOGRAM


# Calculate BMI : weight / height^2
bmi = weight_in_kilograms / (height_in_meters * height_in_meters)

puts "Your calculated BMI is " + bmi.to_s