# Program name:   Asg3bEx1.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     3B
# Exercise:       #1
# Purpose:        To determine the time it will take an object to fall through a given distance


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# constants
ACCELERATION_CONSTANT = 9.81
FEET_PER_METER = 3.28


# get height of the cliff
puts "What is the height of the cliff (in feet)? "
height_in_feet = gets


# convert height to meters
height_in_meters = height_in_feet.to_f / FEET_PER_METER

time = Math.sqrt((2 * height_in_meters) / ACCELERATION_CONSTANT)

puts "It will take the object " + time.to_s + " second(s) to hit the ground."