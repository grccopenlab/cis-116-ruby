# Program name:   Asg3bEx2.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     3B
# Exercise:       #2
# Purpose:        To calculate the maximum height a clown will reach when fired from a cannon at a given angle


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

# constants
FEET_PER_METER = 3.28
V_INITIAL = 40

# get user input
puts "At what angle will the cannon be fired (in degrees)? "
cannon_angle = gets.chomp

# calculate apex height (in meters)
height_in_meters = V_INITIAL * Math.sin(cannon_angle.to_f)

#convert height to feet
height_in_feet = height_in_meters * FEET_PER_METER

# display apex height
puts "The clown will reach a height of " + height_in_feet.to_s + " feet when fired at an angle of " + cannon_angle + " degrees."