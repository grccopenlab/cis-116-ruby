# Program name:   Asg3bEx3.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     3B
# Exercise:       #3
# Purpose:        To determine the remaining balance on a loan


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


puts "What was the original amount of the loan? "
initial_balance = gets

puts "How much is your month payment? "
monthly_payment = gets

puts "How many payments have been made, so far? "
payments_made = gets

puts "What is the annual interest rate of the loan? "
annual_interest_rate = gets

r = (annual_interest_rate.to_f / 12) / 100
n = payments_made.to_i
P = monthly_payment.to_f
bi = initial_balance.to_f

remaining_balance = bi * (1 + r)**(n) - P * ((((1 + r)**n) - 1) / r)

puts "The remaining balance on the loan is $%.2f" % [remaining_balance]
