# Program name:   Asg3bEx4.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     3B
# Exercise:       #4
# Purpose:        To calculate a return on investment


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


puts "What was the original balance invested? "
initial_balance = gets

puts "What is the annual interest rate of the investment? "
annual_interest_rate = gets


r = (annual_interest_rate.to_f / 12) / 100
pv = initial_balance.to_f
N = 12

final_balance = pv * (1 + r)**N

puts "At the end of one year, your investment will be worth $%.2f" % [final_balance]

