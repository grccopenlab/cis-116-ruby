# Program name:   Asg4Ex1.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     4
# Exercise:       #1
# Purpose:        Determine the cost to ship a package, based on weight


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

# constants
LESS_THAN_TWO_COST = 1.10
TWO_TO_SIX_COST = 2.20
SIXPLUS_TO_TEN_COST = 3.70
TENPLUS_COST = 3.80


puts "What is the weight of your package (in pounds)? "
weight = gets

weight_num = weight.to_f

if weight_num <= 0 then
  puts "Weight is not valid."
else

  # format the cost value once
  if weight_num > 10 then
    cost = "$%.2f" % [TENPLUS_COST]
  elsif weight_num > 6 then
    cost = "$%.2f" % [SIXPLUS_TO_TEN_COST]
  elsif weight_num >= 2 then
    cost = "$%.2f" % [TWO_TO_SIX_COST]
  else
    cost = "$%.2f" % [LESS_THAN_TWO_COST]
  end

  # inject formatted cost into message
  puts "It will cost %s to ship your package." % [cost]
end