# Program name:   Asg4Ex2.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     4
# Exercise:       #2
# Purpose:        To determine and apply a discount to an online book order


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

# constants
TEN_TO_NINETEEN_DISCOUNT = 0.20
TWENTY_TO_FORTYNINE_DISCOUNT = 0.30
FIFTY_TO_NINETYNINE_DISCOUNT = 0.40
ONE_HUNDRED_PLUS_DISCOUNT = 0.50


print "How many books will you purchase? "
quantity = gets.chomp()

quantity_num = quantity.to_i

if quantity_num <= 0 then
  puts "Quantity is invalid."
elsif quantity_num < 10 then
  puts "No discounts apply."
else

  print "What is the cost of the book? "
  cost = gets.chomp()

  cost_num = cost.to_f

  if cost_num <= 0 then
    puts "Cost is invalid"
  else
    if quantity_num >= 100 then
      discount_value = "%i" % [ONE_HUNDRED_PLUS_DISCOUNT * 100]
      discount = cost_num * ONE_HUNDRED_PLUS_DISCOUNT

    elsif quantity_num >= 50 then
      discount_value = "%i" % [FIFTY_TO_NINETYNINE_DISCOUNT * 100]
      discount = cost_num * FIFTY_TO_NINETYNINE_DISCOUNT

    elsif quantity_num >= 20 then
      discount_value = "%i" % [TWENTY_TO_FORTYNINE_DISCOUNT * 100]
      discount = cost_num * TWENTY_TO_FORTYNINE_DISCOUNT

    else
      discount_value = "%i" % [TEN_TO_NINETEEN_DISCOUNT * 100]
      discount = cost_num * TEN_TO_NINETEEN_DISCOUNT
    end

    discounted_cost = cost_num - discount

    puts "Your discount rate is %s%" % [discount_value]
    puts "Your discount savings will be $%.2f" % [discount]
    puts "Your discount price will be $%.2f" % [discounted_cost]
  end

end
