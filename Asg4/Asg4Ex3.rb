# Program name:   Asg4Ex3.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     4
# Exercise:       #3
# Purpose:        Calculates a weekly wage based on hours worked and payrate


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

print "How many hours did you work this week? "
hours = gets.chomp()

hours_worked = hours.to_f


print "What is your hourly rate? "
pay_rate = gets.chomp()


# Determine if overtime was earned (hours worked is greater than 40)
if hours_worked > 40 then
  regular_hours = 40
  overtime_hours = hours_worked - 40
else
  regular_hours = hours_worked
  overtime_hours = 0
end

# By assigning zero to overtime hours above, the below calculations can be done exactly
# the same regardless of whether overtime was earned or not; overtime pay is zero if no OT hours
# were worked
rate = pay_rate.to_f
regular_pay = regular_hours * rate
overtime_pay = overtime_hours * (rate * 1.5)

gross_pay = regular_pay + overtime_pay

puts ""
puts "Your gross pay will be $%.2f" % [gross_pay]