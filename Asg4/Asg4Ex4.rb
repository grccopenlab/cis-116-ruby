# Program name:   Asg4Ex4.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     4
# Exercise:       #4
# Purpose:        To determine fines for speeding tickets


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

BASE_FINE = 50.0
EXCESSIVE_SPEED_FINE = 200.00
FINE_PER_HOUR = 5.00

print "At what speed was the driver traveling? (whole numbers only) "
speed = gets.chomp()

driver_speed = speed.to_i


print "What was the posted speed limit? "
limit = gets.chomp()

speed_limit = limit.to_i


if driver_speed > speed_limit then
  # Build up fine amount, starting with the base fine amount
  fine = BASE_FINE

  # Determine how far over the limit the driver was traveling
  difference = driver_speed - speed_limit

  # This is shorthand syntax for fine = fine + (difference * FINE_PER_HOUR)
  fine += (difference * FINE_PER_HOUR)

  if driver_speed > 90 then
    fine += EXCESSIVE_SPEED_FINE
  end

  puts ""
  puts "Driver has exceeded the speed limit."
  puts "Driver will be fined $%.2f by the city." % [fine]

  else
  puts "Driver did not exceed the speed limit."
end