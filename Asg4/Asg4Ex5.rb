# Program name:   Asg4Ex5.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     4
# Exercise:       #5
# Purpose:        To determine the cost of a condo, based on user selections


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

PARK_VIEW = 150000
GOLF_COURSE_VIEW = 170000
LAKE_VIEW = 210000

LOT_PARKING = 0
AWNING_PARKING = 3000
GARAGE_PARKING = 6000


puts "Please select the type of view you would like: "
puts " 1 Park view"
puts " 2 Golf Course view"
puts " 3 Lake view"

print "Your choice: "
choice = gets.chomp()

view_choice = choice.to_i

puts ""
puts "What type of parking would you like: "
puts " 1 Lot"
puts " 2 Covered lot (awning)"
puts " 3 Enclosed garage"

print "Your choice: "
choice = gets.chomp()

parking_choice = choice.to_i


case
  when view_choice == 1
    base_price = PARK_VIEW
    view = "Park view"
  when view_choice == 2
    base_price = GOLF_COURSE_VIEW
    view = "Golf Course view"
  when view_choice == 3
    base_price = LAKE_VIEW
    view = "Lake view"
end


case
  when parking_choice == 1
    parking_price = LOT_PARKING
    parking = "Lot parking"
  when parking_choice == 2
    parking_price = AWNING_PARKING
    parking = "Covered lot (awning) parking"
  when parking_choice == 3
    parking_price = GARAGE_PARKING
    parkig = "Enclosed garage parking"
end

puts ""
puts "Here is the cost of your condo"
puts ""
puts "Choice".ljust(35," ") + "Cost"
puts "-" * 50
puts view.ljust(35, " ") + ("%.2f" % [base_price]).rjust(15, " ")
puts parking.ljust(35, " ") + ("%.2f" % [parking_price]).rjust(15, " ")
puts "=" * 50
puts "Total: ".rjust(35, " ") + ("$%.2f" % [parking_price + base_price]).rjust(15, " ")