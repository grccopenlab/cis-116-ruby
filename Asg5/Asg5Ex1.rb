# Program name:   Asg5Ex1.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     5
# Exercise:       #1
# Purpose:        To prompt a user for a PIN, repeating until correct PIN is entered


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

VALID_PIN = 12345

pin_correct = FALSE

puts "WELCOME TO THE BANK OF PODUNK."

while pin_correct == FALSE
  print "ENTER YOUR PIN: "
  pin = gets.chomp()

  pin_correct = pin.to_i == VALID_PIN
  if !pin_correct then
    puts ""
    puts "INCORRECT PIN. TRY AGAIN."
  end
end

puts ""
puts "PIN ACCEPTED. YOU NOW HAVE ACCESS TO YOUR ACCOUNT."