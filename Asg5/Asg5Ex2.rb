# Program name:   Asg5Ex2.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     5
# Exercise:       #2
# Purpose:        To randomly generate dice rolls until both die values match


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

puts "HERE COMES THE DICE!"
puts ""

dice_match = FALSE

prng = Random.new()

while !dice_match

  # rand(n1, n2) will generate a number between n1 and (n2 -1)
  roll_one = prng.rand(1.. 7)
  roll_two = prng.rand(1..7)

  total = roll_one + roll_two

  puts "Roll #1: %i" % [roll_one]
  puts "Roll #2: %i" % [roll_two]
  puts "The total is %i!" % [total]
  puts ""

  dice_match = (roll_one == roll_two)
end