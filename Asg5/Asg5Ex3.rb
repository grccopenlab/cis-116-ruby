# Program name:   Asg5Ex3.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     5
# Exercise:       #3
# Purpose:        Implements a two-player NIM game


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

pile_A = 3
pile_B = 4
pile_C = 5

player_one_turn = TRUE
game_over = FALSE

print "Player 1, enter your name: "
player_one = gets.chomp()

print "Player 2, enter your name: "
player_two = gets.chomp()

while !game_over

  valid_piles = ["A", "B", "C"]
  valid_pile = FALSE
  valid_take = FALSE

  puts ""
  puts "A: %i B: %i C: %i" % [pile_A, pile_B, pile_C]
  puts ""

  if player_one_turn then
    name = player_one
  else
    name = player_two
  end

  while !valid_pile
    print "%s, choose a pile: " % [name]
    pile = gets.chomp().upcase

    if !valid_piles.include? pile then
      puts "Sorry, %s, that is not a valid pile." % [name]
      puts ""
    else
      case
        when pile == "A"
          valid_pile = pile_A != 0

        when pile == "B"
          valid_pile = pile_B != 0

        when pile == "C"
          valid_pile = pile_C != 0
      end

      if !valid_pile then
        puts "Nice try, %s. That pile is empty. Choose again." % [name]
        puts ""
      end
    end
  end

  while !valid_take
    print "How many to remove from pile %s: " % [pile]
    take = gets.chomp().to_i

    if take > 0 then
      case
        when pile == "A"
          if pile_A >= take then
            pile_A -= take
            valid_take = TRUE
          end

        when pile == "B"
          if pile_B >= take then
            pile_B -= take.to_i
            valid_take = TRUE
          end

        when pile == "C"
          if pile_C >= take then
            pile_C -= take.to_i
            valid_take = TRUE
          end
      end

      if !valid_take then
        puts "Pile %s doesn't have that many. Try again." % [pile]
        puts ""
      end
    else
      puts "You must choose at least 1."
      puts ""
    end
  end

  player_one_turn = !player_one_turn
  game_over = (pile_A + pile_B + pile_C == 0)

end

puts ""
puts "A: %i B: %i C: %i" % [pile_A, pile_B, pile_C]
puts ""

if player_one_turn then
  winner = player_one
else
  winner = player_two
end

puts "%s, there are no counters left, so you WIN!" % [winner]