# Program name:   Asg5Ex4.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     Timothy A. Koets / Kenneth Smith
# Assignment:     5
# Exercise:       #4
# Purpose:        Implements a very basic blackjack game


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

puts "Baby Blackjack!"
puts ""

prng = Random.new

# Player's hand
player_card_1 = prng.rand(1..11)
player_card_2 = prng.rand(1..11)
player_total = player_card_1 + player_card_2

puts "You drew %i and %i." % [player_card_1, player_card_2]
puts "Your total is %i" % [player_total]
puts ""


# Dealer's hand
dealer_card_1 = prng.rand(1..11)
dealer_card_2 = prng.rand(1..11)
dealer_total = dealer_card_1 + dealer_card_2

puts "The dealer has %i and %i." % [dealer_card_1, dealer_card_2]
puts "Dealer's total is %i" % [dealer_total]
puts ""

# Difference will tell us whose hand is greater (or if this is a draw)
difference = player_total - dealer_total

case
  when difference == 0
    puts "DRAW!"

  when difference > 0
    puts "YOU WIN!"

  when difference < 0
    puts "DEALER WINS!"
end
