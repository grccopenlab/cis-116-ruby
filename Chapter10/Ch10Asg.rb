# Program name:   Ch10Asg.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     10
# Purpose:        Demonstrates the creation and use of custom Ruby classes

# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

# require_relative (effectively) loads the external code into this code
require_relative "Order"
require_relative "Online_Order"

# Create an instance of a normal order
order = Order.new(23403, "Jim Smith", 5, 25)
order.display_order()

# Create an instance of an online order
online_order = OnlineOrder.new(34823, "Louise Muller", 8, 40,"Priority Mail", 15)
online_order.display_order()