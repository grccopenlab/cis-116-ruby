# Program name:   online_order.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     10
# Purpose:        Defines a class (a type), OnlineOrder, which descends from the Order class

# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

# require_relative (effectively) loads the external code into this code
require_relative "Order"

class OnlineOrder < Order
  # Constructor
  def initialize(order_number, customer_name, units_ordered, unit_price, shipping_method, shipping_cost)
    super(order_number,customer_name,units_ordered,unit_price)

    @shipping_method = shipping_method
    @shipping_cost = shipping_cost
  end

  # Order cost is the result of the base order cost plus shipping_cost
  def compute_cost()
    return super() + @shipping_cost
  end

  # Displays the details of this order
  def display_order()
    puts "Information for Online Order:"
    puts ""
    puts "Customer Name:".ljust(COLUMN_WIDTH, " ") + ("%s" %[@customer_name])
    puts "Customer Number:".ljust(COLUMN_WIDTH, " ") + ("%s" %[@order_number])
    puts "Number of Products:".ljust(COLUMN_WIDTH, " ") + ("%i" %[@units_ordered])
    puts "Cost per Product:".ljust(COLUMN_WIDTH, " ") + ("$%.2f" %[@unit_price])
    puts "Order Subtotal:".ljust(COLUMN_WIDTH, " ") + "$" + ("%.2f" %[@units_ordered * @unit_price])
    puts "Shipping Method:".ljust(COLUMN_WIDTH, " ") + ("%s" %[@shipping_method])
    puts "Shipping Cost:".ljust(COLUMN_WIDTH, " ") +"$" + ("%.2f" %[@shipping_cost])
    puts "Total Order Cost:".ljust(COLUMN_WIDTH, " ") + "$" + ("%.2f" %[compute_cost()])
    puts ""
  end
end