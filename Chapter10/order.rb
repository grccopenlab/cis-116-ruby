# Program name:   order.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     10
# Purpose:        Defines a class (a type), Order, which encapsulates the details of a customer order

# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

class Order
  COLUMN_WIDTH = 20

  # Constructor
  def initialize(order_number, customer_name, units_ordered, unit_price)
    @order_number = order_number
    @customer_name = customer_name
    @units_ordered = units_ordered
    @unit_price = unit_price
  end

  # Order cost is the result of unit_price times units_ordered
  def compute_cost()
    return @units_ordered * @unit_price
  end

  # Displays the details of this order
  def display_order()
    puts "Information for Order:"
    puts ""
    puts "Customer Name:".ljust(COLUMN_WIDTH, " ") + ("%s" %[@customer_name])
    puts "Customer Number:".ljust(COLUMN_WIDTH, " ") + ("%s" %[@order_number])
    puts "Number of Products:".ljust(COLUMN_WIDTH, " ") + ("%i" %[@units_ordered])
    puts "Cost per Product:".ljust(COLUMN_WIDTH, " ") + "$" + ("%.2f" %[@unit_price])
    puts "Total Order Cost:".ljust(COLUMN_WIDTH, " ") + "$" + ("%.2f" %[compute_cost()])
    puts ""
  end

end