# Program name:   Asg11.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     11
# Purpose:        To demonstrate reading a text file, using the winning teams
#                 of the MLB World Series

# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# load list of winners into an array
def load_winners()
  winners_file = File.open("winners.txt", "r")
  @winners = Array.new

  while (line = winners_file.gets)
    @winners.push(line.strip)
  end
end

# Read winners from text file and display in the console
def display_winning_teams()
  teams_file = File.open("teams.txt", "r")

  puts ""
  puts "The following teams have won the World Series at least once:"
  puts "-" * 65

  while (line = teams_file.gets)
    puts line
  end

  puts ""
end

# prints a message showing the number of wins for the selected team
def get_wins_for_team(team_name)
  # this is a naive search. We aren't looking for similar spellings

  # search for team name and count number of occurrences
  if @winners.include? team_name
    count = 0

    for i in (0..@winners.length)
      if @winners[i] == team_name
        count += 1
      end
    end

    word = "times"
    if count == 1
      word = "time"
    end

    puts ""
    puts ("Team '%s' has won the World Series %i %s" % [team_name, count, word])
    puts ""
  else
    puts ""
    puts "'%s' could not be found" % [team_name]
    puts ""
  end

end


def display_menu()
  choice = 0

  # This is referred to as a "sentinel" value. It is used by the program to
  # basically "watch" for a state that indicates the program should stop looping
  done = FALSE

  # program loop
  while !done
    puts "Please select an option:"
    puts "-" * 35
    puts "1. Show Major League winners"
    puts "2. Show number of wins for a team"
    puts "3. Exit"

    # menu choice loop
    # loop until a valid choice is made
    while choice < 1 or choice > 3
      puts ""
      print "Your choice? "
      choice_input = gets.chomp()
      choice = choice_input.to_i

      if choice < 1 or choice > 3
        puts ""
        puts "Invalid choice. Please try again."
      end
    end

    case choice
      when 1
        display_winning_teams()
      when 2
        puts ""
        print "Which team? "
        team_name = gets
        get_wins_for_team(team_name)
      when 3
        done = TRUE
    end

    # reset choice
    choice = 0
  end

end


load_winners()
display_menu()