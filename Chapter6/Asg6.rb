# Program name:   Asg6.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     6
# Purpose:        To create a word-guessing game, using arrays


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


secret_words = ["joy", "apple", "orphan", "black", "character", "zebra"]

run_again = TRUE

until run_again == FALSE

  # There at least two methods that can used to get a random word:
  # 1.  Use a random number generator to get a random index, and then select that word
  #     from the array:
  #
  #     word_index = rand(secret_words.length)
  #     secret_word = secret_words[word_index]
  #
  # 2.  Use the Array.shuffle function, and then select the first element in the array
  #     Note that we would need to use either shuffle! or assign the array back to itself,
  #     since the operation normally returns a new array (the ! version modifies the original array instead)
  #
  #     secret_words.shuffle!
  #     secret_word = secret_words[0]
  #

  word_index = rand(secret_words.length)
  secret_word = secret_words[word_index]

  # Rather than tracking the used words, I'm simply removing it from the array
  secret_words.delete(secret_word)


  # Use a count-down variable to track incorrect guesses
  guesses_left = 10

  # Keep track of letters that have already been guessed
  guessed_letters = Array.new

  # Track incorrect guesses
  missed_letters = Array.new

  # Create a placeholder 'word' with as many hidden characters as the selected word
  guessed_word = "_" * secret_word.length

  while guesses_left > 0
    # Print the 'game screen'
    if guesses_left == 1
      str = "guess"
    else
      str = "guesses"
    end

    puts ""
    puts "-=" * 15
    puts ""
    puts "You have %i %s remaining." % [guesses_left, str]
    puts ""
    # Using split("") then join(" ") basically injects spaces between each character
    puts "Word: %s" % [guessed_word.split("").join(" ")]
    puts ""
    puts "Misses: %s" % [missed_letters.join("")]
    puts ""

    if guessed_word == secret_word
      puts "YOU GOT IT!"
      puts ""
      # Exit the guessing loop
      break
    else
      invalid_letter = TRUE

      while invalid_letter
        # Prompt for letter
        print "Guess: "
        letter = gets.chomp()

        if letter.length != 1
          puts "Please enter only one letter."
          puts ""
        else
          letter = letter.downcase

          if guessed_letters.include? letter
            puts "You have already used %s." % [letter]
            puts ""
          else
            invalid_letter = FALSE
            guessed_letters.push(letter)

            if secret_word.include? letter
              for i in 0..guessed_word.length - 1
                if secret_word[i] == letter
                  guessed_word[i] = letter
                end
              end
            else
              missed_letters.push(letter)
              guesses_left -= 1
            end
          end
        end
      end
    end
  end

  if guesses_left == 0
    # Assume guesses_left only gets to 0 if user didn't guess the word
    puts ""
    print "No more guesses! The secret word was: %s" % [secret_word]
    puts ""
  end

  # Since we're removing words from the 'list', exit if we've used them all
  if secret_words.length == 0
    puts ""
    puts "No more words to guess!"
    run_again = FALSE
  else
    # Ask the user if they want to keep playing
    puts ""
    print "Play again (y) or quit (q)? "
    choice = gets.chomp().downcase

    run_again = (choice == "y")
  end
end

# End game - always display this message
puts ""
puts "Thank you for playing!"