# Program name:   Asg7.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     7
# Purpose:        To demonstrate the typical sort times for various sorting algorithms


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# Selection Sort
def getElapsedTimeForSelectionSort(numbers)
  print "Selection Algorithm..."
  elapsed_time = 0

  # Start the 'timer'
  start_time = Time.now.to_f

  count = numbers.length

  # This is the actual selection sort
  for i in (0..count - 2)
    # Assume the current value of i is the lowest number
    min_position = i

    # Check every element in the array past the i index
    for j in (i + 1)..(count - 1)
      # If the value at index j is lower, make min_position the value of j
      if numbers[j] < numbers[min_position]
        min_position = j
      end
    end

    # Swap
    temp = numbers[i]
    numbers[i] = numbers[min_position]
    numbers[min_position] = temp
  end

  # Stop the 'timer'
  end_time = Time.now.to_f

  # Get the difference, in seconds
  diff = end_time - start_time

  # Convert to milliseconds (elapsed time will normally be a very small number)
  elapsed_time = diff * 1000

  puts "done."

  return elapsed_time
end


# Insertion Sort
def getElapsedTimeForInsertionSort(numbers)
  print "Insertion Algorithm..."
  elapsed_time = 0

  # Start the 'timer'
  start_time = Time.now.to_f

  count = numbers.length

  # This is the actual insertion sort
  for i in (0..count - 1)
    # Set the current index to i
    current = i

    # Sentinel value for inner loop
    done = FALSE

    # Test each number from the current index
    # If the current number is less than the previous number, swap the numbers.
    # Otherwise, move on to the next number
    while (current > 0 and !done)
      if numbers[current] < numbers[current - 1]
        # Swap
        temp = numbers[current  - 1]
        numbers[current - 1] = numbers[current]
        numbers[current] = temp
      else
        done = TRUE
      end

      current -= 1
    end
  end

  # Stop the 'timer'
  end_time = Time.now.to_f

  # Get the difference, in seconds
  diff = end_time - start_time

  # Convert to milliseconds (elapsed time will normally be a very small number)
  elapsed_time = diff * 1000

  puts "done."

  return elapsed_time
end


# Bubble Sort
def getElapsedTimeForBubbleSort(numbers)
  print "Bubble Algorithm..."
  elapsed_time = 0

  # Start the 'timer'
  start_time = Time.now.to_f

  count = numbers.length

  # This is the actual bubble sort

  # Loop until the second-to-last number - the last number is assumed to be in the
  # correct index by the time the loop gets to that index
  for outer_index in (0..count - 2)
    # Start the inner loop at the current index of the outer loop
    for inner_index in ((outer_index + 1)..count - 1)
      if numbers[outer_index] > numbers[inner_index]
        # Swap
        temp = numbers[inner_index]
        numbers[inner_index] = numbers[outer_index]
        numbers[outer_index] = temp
      end
    end
  end

  # Stop the 'timer'
  end_time = Time.now.to_f

  # Get the difference, in seconds
  diff = end_time - start_time

  # Convert to milliseconds (elapsed time will normally be a very small number)
  elapsed_time = diff * 1000

  puts "done."

  return elapsed_time
end


# Radix Sort
def getElapsedTimeForRadixSort(numbers)
  print "Radix Algorithm..."
  elapsed_time = 0

  max_length = 0
  count = numbers.length

  # First, we need to 0-pad the numbers, to make it easier to sort them into 'buckets'

  # We need to know what the maximum number of digits will be
  for i in 0..count
    length = numbers[i].to_s.length
    if length > max_length
      max_length = length
    end
  end

  # Now that we know how many digits we need to pad, we'll create a new array with the padded numbers
  padded_numbers = Array.new

  for i in 0..count
    padded_numbers.push(numbers[i].to_s.rjust(max_length,"0"))
  end

  # NOW we can finally start the sorting algorithm...

  # Start the 'timer'
  start_time = Time.now.to_f

  # This is actual radix sort

  # max_length tells us how many 'buckets' we need
  for outer_index in (0..max_length - 1)
    buckets = Hash.new()

    # There are only 10 digits...
    for n in (0..9)
      buckets[n.to_s] = Array.new
    end

    for inner_index in (0..count - 1)
      num = padded_numbers[inner_index]
      digit = num[max_length - 1 - outer_index]

      buckets[digit].push(num)
    end

    numbers = buckets.values.flatten
  end

  # Stop the 'timer'
  end_time = Time.now.to_f

  # Get the difference, in seconds
  diff = end_time - start_time

  # Convert to milliseconds (elapsed time will normally be a very small number)
  elapsed_time = diff * 1000

  puts "done."

  return elapsed_time
end


# Built-in Ruby Sort
def getElapsedTimeForRubySort(numbers)
  print "Ruby Algorithm..."
  elapsed_time = 0

  # Start the 'timer'
  start_time = Time.now.to_f

  # Calling the Ruby sort! function
  # sort! sorts the algoritm in-place (it modifies the array on which it is called)
  # sort (note the missing '!') sorts the array and returns a copy (it does not modify the source array)
  numbers.sort!

  # Stop the 'timer'
  end_time = Time.now.to_f

  # Get the difference, in seconds
  diff = end_time - start_time

  # Convert to milliseconds (elapsed time will normally be a very small number)
  elapsed_time = diff * 1000

  puts "done."

  return elapsed_time
end


# Prompt use for number of random numbers to generate
def getNumberOfElementsToGenerate()
  valid_num = FALSE

  # loop until user gives us a valid number of elements to generate
  until valid_num
    print "How many values do you wish to sort? "
    num = gets.chomp()

    count = num.to_i

    valid_num = (count > 0)

    if !valid_num
      puts ""
      puts "'%s' is not a valid number. Please enter a value greater than 0."
      puts ""
    end
  end

  # If we're here, we have a valid number of elements to generate
  return count
end


# Prompt user for the maximum number to generate
def getUpperLimit()
  valid_num = FALSE

  # loop until user gives us a valid limit value
  until valid_num
    print "What is the upper limit of the values to be sorted? "
    num = gets.chomp()

    limit = num.to_i

    valid_num = (limit > 0)

    if !valid_num
      puts ""
      puts "'%s' is not a valid number. Please enter a value greater than 0."
      puts ""
    end
  end

  # If we're here, we have a valid limit value
  return limit
end


# The 'main' function
def runProgram()
  count = getNumberOfElementsToGenerate()
  limit = getUpperLimit()

  # Calling Array.new this way gives us an array of the size indicated
  # The {} notation means we're populating the array with the values inside;
  # in this case, a range of random numbers between 0 and the limit, exclusive
  random_numbers = Array.new(count){ rand(1..limit) }

  # Record our timer results in an array (to make sorting easier at the end)
  timings = Array.new

  puts ""

  # Array.clone() duplicates the array
  timings.push(["Insertion", getElapsedTimeForInsertionSort(random_numbers.clone())])
  timings.push(["Bubble", getElapsedTimeForBubbleSort(random_numbers.clone())])
  timings.push(["Selection", getElapsedTimeForSelectionSort(random_numbers.clone())])
  timings.push(["Radix", getElapsedTimeForRadixSort(random_numbers.clone())])
  timings.push(["Ruby Array Sort Method", getElapsedTimeForRubySort(random_numbers.clone())])

  # Sort the timings array
  # This tells the sort! algorithm to sort by the second value in each row
  timings.sort!{|a,b| b[1] <=> a[1]}

  # Print the results
  puts ""
  puts "=" * 50
  puts "Algorithm Analysis"
  puts "Data Set: %i integers between 0 and %i" % [count, limit]
  puts "=" * 50
  puts "%-35s %-14s" % ["Algorithm", "Duration"]
  puts "-" * 50
  for i in 0..4
    time = "%.4f" % [timings[i][1]]
    puts "%-35s %s ms" % [timings[i][0], time.rjust(11, " ")]
  end
  puts "-" * 50
end


# Now that we've encapsulated all of our logic, run the "main" function
runProgram()