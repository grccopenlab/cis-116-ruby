# Program name:   Ch8Asg.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     8
# Purpose:        To allow a user to determine the time to break even on the
#                 cost of a plumbing upgrade, in years

# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)


# Two-dimensional array to store before and after water bills
bills = []

# Months as strings
months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

is_valid = FALSE

# Repeat this loop until is_valid is true
until is_valid == TRUE
  print "Please enter the cost of upgrading: "
  cost_to_upgrade = gets.chomp()

  is_valid = (cost_to_upgrade.to_i > 0)

  if(!is_valid)
    puts cost_to_upgrade + " is not a number. Please only enter numeric values."
    puts ""
  end
end

# If we're here, we have a valid upgrade cost. now, let's get the before and after costs
print "Please enter the bill for "

# loop through 12 months
for i in (0..11)
  puts ""
  puts months[i]

  is_valid = FALSE

  until is_valid == TRUE
    print "Before: "
    bill_before = gets.chomp()

    is_valid = (bill_before.to_i > 0)

    if(!is_valid)
      puts bill_before + " is not a number. Please only enter numeric values."
      puts ""
    else
      before_sum += bill_before.to_f
    end
  end

  is_valid = FALSE

  until is_valid == TRUE
    print "After: "
    bill_after = gets.chomp()

    is_valid = (bill_after.to_i > 0)

    if(!is_valid)
      puts bill_after + " is not a number. Please only enter numeric values."
      puts ""
    else
      after_sum += bill_after.to_f
    end
  end

  # Calculate the difference between the before and after values
  bill_diff = bill_before.to_f - bill_after.to_f

  # Accumulate the difference
  diff_sum += bill_diff

  # Add this month's value to the end of the array
  # I'm including the month name to simplify the sorted display later
  bills.push([months[i], bill_before, bill_after, bill_diff])
end

# Savings, in date order, by month
puts ""
puts "Savings by Month, Chronological"

puts ""
puts "Month".ljust(12, " ")+ " | " + "Before".ljust(12, " ") + " | " + "After".ljust(12, " ")+ " | " + "Difference"
puts "=" * 60

for i in (0..11)
  puts bills[i][0].ljust(12, " ")+ " | " + ("%.2f" % bills[i][1]).rjust(12, " ")+ " | " + ("%.2f" % bills[i][2]).rjust(12, " ")+ " | " + ("%.2f" % bills[i][3]).rjust(15, " ")
end

puts "-" * 60
puts "Totals".ljust(12, " ")+ " | " + ("$%.2f" % before_sum).rjust(12, " ")+ " | " + ("$%.2f" % after_sum).rjust(12, " ")+ " | " + ("$%.2f" % diff_sum).rjust(15, " ")
puts "=" * 60

# This will sort the array by comparing the 4th item in the row (the difference between before and after)
bills = bills.sort{|a,b| b[3] <=> a[3]}

# Savings, in descending order of savings, by month
puts ""
puts "Savings by Month, Highest to Lowest"

puts ""
puts "Month".ljust(12, " ")+ " | " + "Before".ljust(12, " ") + " | " + "After".ljust(12, " ")+ " | " + "Difference"
puts "=" * 60

for i in (0..11)
  puts bills[i][0].ljust(12, " ")+ " | " + ("%.2f" % bills[i][1]).rjust(12, " ")+ " | " + ("%.2f" % bills[i][2]).rjust(12, " ")+ " | " + ("%.2f" % bills[i][3]).rjust(15, " ")
end

puts "-" * 60
puts "Totals".ljust(12, " ")+ " | " + ("$%.2f" % before_sum).rjust(12, " ")+ " | " + ("$%.2f" % after_sum).rjust(12, " ")+ " | " + ("$%.2f" % diff_sum).rjust(15, " ")
puts "=" * 60

# Calculate and display the number of years it will take to break even on the upgrade
puts ""
print "It would take approximately %i years to pay off the upgrade." % (cost_to_upgrade.to_f / diff_sum).to_i