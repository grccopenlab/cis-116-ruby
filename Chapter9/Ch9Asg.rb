# Program name:   Ch9Asg.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     9
# Purpose:        To demonstrate the use of classes and functions


# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

require_relative "Product"

def print_menu()
  puts "Please selection an option:"
  puts "-"*30
  puts "1 Create a new product"
  puts "2 Look up a product"
  puts "3 Process a product purchase"
  puts "4 Exit"
  puts "" # blank line, for spacing
end


def create_new_product(products)
  puts ""
  puts "Creating new product..."

  done = FALSE

  num = ""
  desc = ""
  quantity = ""
  cost = ""
  retail = ""

  while done == FALSE
    print "Product number? "
    num = gets.chomp

    test = find_product(num, products)
    if test == -1
      print "Product description? "
      desc = gets.chomp

      print "Quantity on hand? "
      quantity = gets.chomp

      while quantity.to_i == 0
        puts "Invalid quantity."

        print "Quantity on hand? "
        quantity = gets.chomp
      end

      print "Cost? "
      cost = gets.chomp

      while cost.to_f == 0
        puts "Invalid cost."

        print "Cost? "
        cost = gets.chomp
      end

      print "Retail? "
      retail = gets.chomp

      while retail.to_f == 0
        puts "Invalid retail."

        print "Retail? "
        retail = gets.chomp
      end

      product = Product.new(num, desc, cost.to_f, retail.to_f, quantity.to_i)

      products.push(product)
      done = TRUE
      puts "Product [" + desc + "] added successfully!"
      puts ""
    else
      puts "Product number is already in use. Please enter a new product number."
    end
  end
end


def lookup_product(product_number, products)
  index = find_product(product_number, products)

  if index == -1
    puts "Product not found!"
    puts ""
  else
    puts ""
    puts "Product found!"
    puts "-"*30
    products[index].display()
    puts ""
  end
end


def find_product(product_number, products)
  index = -1

  if products.length > 0
    for i in 0..products.length - 1
      if products[i].product_number == product_number
        index = i
        break
      end
    end
  end

  return index
end


def process_product_purchase(products)
  puts "The following products are available. Please enter the product number you wish to purchase:"
  puts ""

  puts "%-10s %-20s %-10s %-10s" % ["#", "Name", "Qty.", "Cost"]
  puts "-" * 50
  for i in 0..products.length - 1
    item = products[i]
    if item.quantity > 0
      puts "%-10s %-20s %-10s %-10s" % [item.product_number, item.description, item.quantity, item.retail]
    end
  end
  puts "=" * 50

  puts ""
  print "Product #? "
  product_number = gets.chomp()

  if product_number != ""
    index = find_product(product_number, products)

    if index == -1
      puts "Product not found!"
      puts ""
    else
      item = products[i]

      quantity = 0
      valid = FALSE
      while valid == FALSE
        print "How many? "
        num_input = gets.chomp()

        quantity = num_input.to_i

        if quantity < 1 or quantity > item.quantity
          puts "Invalid quantity. Please enter %i or less." % [item.quantity]
        else
          valid = TRUE
          products[i].remove_quantity_from_stock(quantity)

          display_product_purchase(item, quantity)
        end
      end
    end
  end
end


def display_product_purchase(product, quantity)
  subtotal = product.retail * quantity
  tax = subtotal * 0.06
  total = subtotal + tax

  puts ""
  puts "Your purchase order:"
  puts "-" * 50
  puts "Product #:".ljust(15, " ") + product.product_number
  puts "Description:".ljust(15, " ") + product.description
  puts "Quantity:".ljust(15, " ") + quantity.to_s.rjust(35, " ")
  puts "Subtotal:".ljust(15, " ") + ("%.2f" % [subtotal]).rjust(35, " ")
  puts "Tax:".ljust(15, " ") + ("%.2f" % [tax]).rjust(35, " ")
  puts "-" * 50
  puts "Total:".ljust(15, " ") + ("$ %.2f" % [total]).rjust(35, " ")
  puts "=" * 50
  puts ""
end


def run_program()
  inventory = Array.new
  choice = 0

  while choice != 4
    print_menu()

    print "Your choice? "
    input = gets.chomp

    # this will return a zero if user enters a non-number value
    choice = input.to_i

    if (choice < 1) or (choice > 4)
      puts "Invalid choice. Please try again"
      puts ""
    else
      # use switch/case statement to determine appropriate action
      case choice
        when 1
          # create
          create_new_product(inventory)
        when 2
          # lookup
          print "Product number? "
          num = gets.chomp
          lookup_product(num, inventory)
        when 3
          # process purchase
          process_product_purchase(inventory)
        when 4
          # exit
          puts "Thank you! Program will now exit."
      end
    end
  end
end


run_program()