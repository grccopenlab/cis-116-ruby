# Program name:   product.rb
# Name:           Open Lab Tutor
# Date:           ---
# Subject:        CIS 116 Computer Programming
# Instructor:     ---
# Assignment:     9
# Purpose:        To encapsulate the details of a Product

# STUDENTS SHOULD DEFINE THEIR ALGORITHM HERE (psuedo-code, basically)

class Product
  # constructor
  def initialize(product_number, description, cost, retail, quantity_on_hand)
    @product_number = product_number
    @quantity_on_hand = quantity_on_hand
    @description = description
    @cost = cost
    @retail = retail
  end

  def product_number
    return @product_number
  end

  def description
    return @description
  end

  def quantity
    return @quantity_on_hand
  end

  def retail
    return @retail
  end

  def remove_quantity_from_stock(number_sold)
    @quantity_on_hand -= number_sold
  end

  # prints Product values to the console in a two-column format
  def display()
    puts "Product #:".ljust(20, " ") + @product_number
    puts "Description:".ljust(20, " ") + @description
    puts "Quantity on hand:".ljust(20, " ") + @quantity_on_hand.to_s
    puts "Cost:".ljust(20, " ") + ("$ %.2f" % [@cost])
    puts "Retail price:".ljust(20, " ") + ("$ %.2f" % [@retail])
  end
end