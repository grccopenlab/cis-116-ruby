### Requirements

RubyMine, available at https://www.jetbrains.com/ruby/, or a compatible IDE.

Ruby, version 2.4 or higher, available at https://www.ruby-lang.org/en/downloads/

"Computer Science Programming Basics in Ruby", by Ophir Frieder, Gideon Frieder, and David Grossman. 
Published by O'Reily Media.
ISBN: 978-1-449-35597-5

eBook is generally made available for free by the instructor, via Blackboard.